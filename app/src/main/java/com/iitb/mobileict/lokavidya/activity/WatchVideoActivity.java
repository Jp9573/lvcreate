package com.iitb.mobileict.lokavidya.activity;

import android.content.Context;
import android.net.Uri;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.iitb.mobileict.lokavidya.R;
import com.iitb.mobileict.lokavidya.util.Master;

import java.io.File;

import tcking.github.com.giraffeplayer2.VideoView;

public class WatchVideoActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_watch_video);

        VideoView videoView = findViewById(R.id.video_view);

        final String path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/lokavidya" + "/" + Master.getCurrentProjectName() + "/" + "Video.mp4";
        File file = new File(path);
        if(file != null) {
            Uri videoUri = Uri.fromFile(file);

            videoView.setVideoPath(videoUri.toString()).getPlayer().start();
            videoView.getPlayer().pause();
        }else {
            Toast.makeText(this, "Video not found!", Toast.LENGTH_LONG).show();
        }
    }
}
