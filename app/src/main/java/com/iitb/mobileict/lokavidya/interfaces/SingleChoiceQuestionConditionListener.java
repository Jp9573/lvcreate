package com.iitb.mobileict.lokavidya.interfaces;

/**
 * Created by jay on 12/3/18.
 */

public interface SingleChoiceQuestionConditionListener {
    public void conditionSatisfied(int index);
    public void conditionFailed(int index);
}
