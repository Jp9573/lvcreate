package com.iitb.mobileict.lokavidya.util;

import android.app.ProgressDialog;
import android.content.Context;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jay on 28/2/18.
 */

public class Master {

    private static final String serverURL = "http://ssonew.lokavidya.com/api";

    public static String getLoginAPI() {
        return serverURL + "/login";
    }

    public static String getRegistrationAPI() {
        return serverURL + "/signup";
    }

    public static String getForgotPasswordAPI() {
        return serverURL + "/forgot";
    }

    public static String getOTPCheckAPI() {
        return serverURL + "/check";
    }

    public static String getResetPasswordAPI() {
        return serverURL + "/reset";
    }

    private static ProgressDialog pDialog;

    private static String currentProjectName;

    public static String getCurrentProjectName() {
        return currentProjectName;
    }

    public static void setCurrentProjectName(String currentProjectName) {
        Master.currentProjectName = currentProjectName;
    }

    public static String AUTH_USERNAME = "lokacart@cse.iitb.ac.in", AUTH_PASSWORD = "password";

    public static String AppName = "LVCreate";
    public static String AppPath = "/LVCreate" ;
    public static String ProjectsPath = "/Projects";
    public static String MyProjectsPath="/MyProjects";
    public static String SavedProjectsPath="/SavedProjects";
    public static String VideosPath = "/Videos";
    public static String SavedVideosPath="/SavedVideos";
    public static String ImagesPath ="/images";
    public static String AudiosPath ="/audios";

    public static List<String> imageNames = new ArrayList<>();
    public static List<String> audioNames = new ArrayList<>();
    public static List<String> videosNames = new ArrayList<>();

    public static void showProgressDialog(Context context, String message) {
        if (pDialog != null && pDialog.isShowing())
            pDialog.dismiss();
        pDialog = new ProgressDialog(context);
        pDialog.setMessage(message);
        pDialog.setCancelable(false);
        pDialog.show();
    }

    public static void setProgress(String progress) {
        if (pDialog != null && pDialog.isShowing()) {
            //pDialog.setProgress(progress);
            pDialog.setMessage(progress);
        }
    }

    public static void dismissProgressDialog() {
        if (pDialog != null && pDialog.isShowing())
            pDialog.dismiss();
    }
}
