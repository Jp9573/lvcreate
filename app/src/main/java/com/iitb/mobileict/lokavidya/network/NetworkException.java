package com.iitb.mobileict.lokavidya.network;


public class NetworkException extends Exception {

    public NetworkException() {}

    public NetworkException(String message){
        super(message);
    }
}
