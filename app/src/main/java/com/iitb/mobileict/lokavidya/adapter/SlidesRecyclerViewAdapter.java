package com.iitb.mobileict.lokavidya.adapter;


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import com.iitb.mobileict.lokavidya.Projectfile;
import com.iitb.mobileict.lokavidya.R;
import com.iitb.mobileict.lokavidya.dialog.SingleChoiceQuestionDialog;
import com.iitb.mobileict.lokavidya.fragment.CreateProjectFragment;
import com.iitb.mobileict.lokavidya.util.Master;

import java.io.File;

/**
 * Created by jay on 6/3/18.
 */

public class SlidesRecyclerViewAdapter extends RecyclerView.Adapter<SlidesRecyclerViewAdapter.MyViewHolder> {

    Context ctx;
    Fragment mFragment;
    Projectfile projectfile;
    int count = 0;

    public SlidesRecyclerViewAdapter(Context context, CreateProjectFragment fragment, Projectfile f) {
        ctx = context;
        mFragment = fragment;
        projectfile = f;
        if(f != null) {
            Master.imageNames = f.getImageNames(Master.getCurrentProjectName());
            Master.audioNames = f.getAudioNames(Master.getCurrentProjectName());
        }
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        public ImageView imageview;
        public ImageButton delete;

        public MyViewHolder(View v) {
            super(v);

            imageview = v.findViewById(R.id.thumbImage);
            imageview.setScaleType(ImageView.ScaleType.CENTER_CROP);
            delete = v.findViewById(R.id.deleteImageButton);

        }
    }

    @Override
    public SlidesRecyclerViewAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.galleryitem, parent, false);
        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        //GalleryItem p = objects.get(position);

        File image_file = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/lokavidya" + "/" + Master.getCurrentProjectName() + "/images", Master.imageNames.get(position) /*+ ".png"*/);

        String name = Master.imageNames.get(position);
        String extention = name.substring(name.length() - 3);

        Bitmap myBitmap = null;
        String videoPath = null;
        Boolean isJson = false;
        Boolean isAudio = false;

        switch (extention) {
            case "png":
                myBitmap = BitmapFactory.decodeFile(image_file.getAbsolutePath());
                holder.imageview.setImageBitmap(myBitmap);
                break;
            case "mp4":
                videoPath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + "lokavidya" + "/" + Master.getCurrentProjectName() + "/images/" + Master.imageNames.get(position);
                Bitmap thumb = ThumbnailUtils.createVideoThumbnail(videoPath, MediaStore.Video.Thumbnails.MINI_KIND);
                holder.imageview.setImageBitmap(thumb);
                break;
            case "son":
                //Bitmap quizImg = BitmapFactory.decodeResource(ctx.getResources(), R.drawable.ic_quiz_black_24dp);
                holder.imageview.setImageResource(R.drawable.ic_quiz_black_24dp);
                isJson = true;
                break;
            case "wav":
                holder.imageview.setImageResource(R.drawable.logo_circle);
                isAudio = true;
                break;
            default:
                Toast.makeText(ctx, "Error in loading " + position, Toast.LENGTH_LONG).show();
        }


        final Bitmap finalMyBitmap = myBitmap;
        final String finalVideoPath = videoPath;
        final Boolean finalIsJson = isJson;
        final Boolean finalIsAudio = isAudio;
        holder.imageview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(finalMyBitmap != null) {
                    ClickCallback clickCallback = (ClickCallback) mFragment;
                    clickCallback.ImageClickCallBack(position);
                }else if (finalIsJson) {
                    SingleChoiceQuestionDialog dialog = new SingleChoiceQuestionDialog(ctx, mFragment, Master.imageNames.get(position));
                    dialog.show();
                    Window window = dialog.getWindow();
                    window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                }else if (finalIsAudio) {
                    ClickCallback clickCallback = (ClickCallback) mFragment;
                    clickCallback.ImageClickCallBack(position);
                } else {
                    if(finalVideoPath != null) {
                        File file = new File(finalVideoPath);
                        Uri uri = Uri.fromFile(file);
                        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                        intent.setDataAndType(uri, "video/mp4");
                        mFragment.startActivity(intent);
                    }
                }
            }
        });

        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final AlertDialog.Builder builder1 = new AlertDialog.Builder(ctx);
                builder1.setTitle("Delete?");
                builder1.setMessage("Are you sure you want to delete this slide?");
                builder1.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        deleteSlide(Master.imageNames.get(position));
                    }
                }) .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });

                builder1.create().show();
            }
        });
    }

    void deleteSlide(String name) {
        File file = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/lokavidya" + "/" + Master.getCurrentProjectName() + "/images", name);
        if(file.delete()) {
            Master.imageNames.remove(name);
            Toast.makeText(ctx, "Slide deleted!", Toast.LENGTH_LONG).show();
        }else {
            Toast.makeText(ctx, "Theres's a Problem in deletion!", Toast.LENGTH_LONG).show();
        }
        mFragment.onActivityResult(544, 544, null);
    }

    @Override
    public int getItemCount() {
        return Master.imageNames.size();
    }

    public interface ClickCallback {
        void ImageClickCallBack(int position);
    }
}
