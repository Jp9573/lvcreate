package com.iitb.mobileict.lokavidya.dialog;

import android.app.Dialog;
import android.content.Intent;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.iitb.mobileict.lokavidya.R;
import com.iitb.mobileict.lokavidya.interfaces.SingleChoiceQuestionConditionListener;
import com.iitb.mobileict.lokavidya.listeners.MinimumConditionOnTextChangeListener;
import com.iitb.mobileict.lokavidya.util.Master;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by jay on 12/3/18.
 */

public class SingleChoiceQuestionDialog extends Dialog implements SingleChoiceQuestionConditionListener {

    private static final int MAX_OPTIONS_COUNT = 4;
    private static final int SHOULD_ALLOW_DELETE_MIN = 2;

    private Context mContext;
    private Fragment mFragment;
    private String fName = null;

    private EditText mQuestionStringEditText;
    private RadioGroup mQuestionOptionsRadioGroup;
    private ImageButton mAddOptionImageButton;
    private Button mDoneButton;


    private int mSlideNumber;
    public static ArrayList<Boolean> mConditionSatisfiesStackList = new ArrayList<>();

    public SingleChoiceQuestionDialog(Context context, Fragment mFragment) {
        super(context);
        mContext = context;
        this.mFragment = mFragment;
    }

    public SingleChoiceQuestionDialog(Context context, Fragment mFragment, String fileName) {
        super(context);
        mContext = context;
        this.mFragment = mFragment;
        fName = fileName;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Setup UI
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.single_choice_question_layout);


        // Initialize UI Components
        mQuestionStringEditText = (EditText) findViewById(R.id.question_string_edit_text);
        mQuestionOptionsRadioGroup = (RadioGroup) (findViewById(R.id.options).findViewById(R.id.radio_group));
        mAddOptionImageButton = (ImageButton) findViewById(R.id.add_option_image_button);
        mDoneButton = (Button) findViewById(R.id.done_button);

        if (fName != null) {
            populateData();
        }

        mAddOptionImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addOption();
            }
        });

        mDoneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ArrayList<String> options = new ArrayList<String>();
                for (int i = 0; i < mQuestionOptionsRadioGroup.getChildCount(); i++) {
                    options.add(((EditText) mQuestionOptionsRadioGroup.getChildAt(i).findViewById(R.id.edit_text)).getText().toString());
                }

                ArrayList<Integer> correctOptions = new ArrayList<Integer>();
                for (int i = 0; i < mQuestionOptionsRadioGroup.getChildCount(); i++) {
                    if (((RadioButton) mQuestionOptionsRadioGroup.getChildAt(i).findViewById(R.id.radio_button)).isChecked()) {
                        correctOptions.add(i);
                    }
                }
                /*mQuestionDoneListener.onDone(
                        mQuestionStringEditText.getText().toString(),
                        options,
                        correctOptions,
                        mSlideNumber
                );*/
                if(correctOptions.size() > 0) {
                    convertAndSaveToJSON(mQuestionStringEditText.getText().toString(), options, correctOptions);

                    dismiss();
                }else {
                    Toast.makeText(getContext(), "Please select any one right answer among all options!", Toast.LENGTH_LONG).show();
                }
            }
        });
        //calculateDoneButtonEnabled();
    }

    void populateData() {
        String json = null;
        File file = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/lokavidya/" + Master.getCurrentProjectName() + "/images/" + fName);
        if (file != null) {
            try {
                FileInputStream fileInputStream = new FileInputStream(file);//mContext.openFileInput(fName);
                InputStreamReader inputStreamReader = new InputStreamReader(fileInputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                StringBuilder stringBuilder = new StringBuilder();
                String line;
                while ((line = bufferedReader.readLine()) != null) {
                    stringBuilder.append(line);
                }
                json = stringBuilder.toString();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if (json != null) {
            try {
                JSONObject parentJsonObject = new JSONObject(json);
                JSONObject quizObject = parentJsonObject.getJSONObject("quiz");
                JSONArray questions = quizObject.getJSONArray("questions");
                JSONObject object = questions.getJSONObject(0);
                String que = object.get("question").toString();
                String answerID = "0";
                if(!object.getJSONArray("answer").isNull(0)) {
                    answerID = object.getJSONArray("answer").getJSONObject(0).get("id").toString();
                }
                List<String> options = new ArrayList<>();
                JSONArray optionsJson = object.getJSONArray("options");

                mQuestionStringEditText.setText(que);
                for (int i = 0; i < optionsJson.length(); i++) {
                    JSONObject jo = optionsJson.getJSONObject(i);
                    options.add(jo.get("option").toString());

                    if (Integer.valueOf(answerID) == (i + 1)) {
                        addOption(true, true, options.get(i));
                    } else {
                        addOption(true, false, options.get(i));
                    }
                }


            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    void convertAndSaveToJSON(String que, ArrayList<String> options, ArrayList<Integer> correctOptions) {
        JSONObject parentJsonObject = new JSONObject();
        try {
            JSONObject quizObject = new JSONObject();
            JSONArray questions = new JSONArray();
            JSONObject object = new JSONObject();
            object.put("id", getQuizId());
            object.put("type", "mcq");
            object.put("time", "10");
            object.put("question", que);
            object.put("skippable", true);
            object.put("Hint", "");

            JSONArray answerJsonArray = new JSONArray();
            for (int i = 0; i < correctOptions.size(); i++) {
                JSONObject jo = new JSONObject();
                jo.put("id", correctOptions.get(i)+1);
                jo.put("option", options.get(correctOptions.get(i)));
                answerJsonArray.put(jo);
            }
            object.put("answer", answerJsonArray);

            JSONArray optionsJsonArray = new JSONArray();
            for (int i = 0; i < options.size(); i++) {
                JSONObject jo = new JSONObject();
                jo.put("id", i + 1);
                jo.put("option", options.get(i));
                optionsJsonArray.put(jo);
            }
            object.put("options", optionsJsonArray);

            questions.put(object);
            quizObject.put("questions", questions);
            parentJsonObject.put("quiz", quizObject);

            saveQuiz(parentJsonObject);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void saveQuiz(JSONObject object) {
        if(fName == null) {
            String newImg;
            File sdCard = Environment.getExternalStorageDirectory();
            File imgDir = new File(sdCard.getAbsolutePath() + "/lokavidya/" + Master.getCurrentProjectName() + "/images");

            if (imgDir.exists() && imgDir.isDirectory()) {
                File file[] = imgDir.listFiles();
                newImg = Master.getCurrentProjectName() + "." + String.valueOf(file.length + 1) + ".json";

                if (file.length != 0) {
                    boolean fileExists = false;

                    int countImg[] = new int[file.length];

                    for (int i = 0; i < file.length; i++) {
                        String imgName = file[i].getName();
                        imgName = imgName.replace(".json", "");
                        String splitImgName[] = imgName.split("\\.");
                        countImg[i] = Integer.parseInt(splitImgName[1]);
                        if (newImg.equals(file[i].getName())) {
                            fileExists = true;
                        }

                    }

                    if (fileExists == true) {
                        int large = largestAndSmallest(countImg);

                        newImg = Master.getCurrentProjectName() + "." + String.valueOf(large + 1) + ".json";
                    }
                }
                File writetofile = new File(imgDir, newImg);
                FileOutputStream outStream = null;

                try {
                    outStream = new FileOutputStream(writetofile);

                    outStream.write(object.toString().getBytes());

                    outStream.flush();
                    outStream.close();

                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    try {
                        if (outStream != null) {
                            outStream.close();
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

            } else {
                imgDir.mkdirs();
                saveQuiz(object);
            }

            mFragment.onActivityResult(544, 544, null);
        }else {
            File file = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/lokavidya/" + Master.getCurrentProjectName() + "/images/" + fName);
            if (file != null) {
                try {
                    FileOutputStream fileOutputStream = new FileOutputStream(file);
                    fileOutputStream.write(object.toString().getBytes());

                    fileOutputStream.flush();
                    fileOutputStream.close();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    public String getQuizId() {
        String newImg;
        File sdCard = Environment.getExternalStorageDirectory();
        File imgDir = new File(sdCard.getAbsolutePath() + "/" + "lokavidya" + "/" + Master.getCurrentProjectName() + "/images");

        if (!imgDir.exists()) {
            imgDir.mkdirs();
        }

        File file[] = imgDir.listFiles();
        newImg = String.valueOf(file.length + 1);

        if (file.length != 0) {
            boolean fileExists = false;

            int countImg[] = new int[file.length];

            for (int i = 0; i < file.length; i++) {
                String imgName = file[i].getName();
                Log.i("addimage imgname", imgName);
                imgName = imgName.replace(".mp4", "");
                Log.i("addimage imgname 2", imgName);
                String splitImgName[] = imgName.split("\\.");
                try {
                    countImg[i] = Integer.parseInt(splitImgName[1]);
                }catch (Exception e) {
                    countImg[i] = Integer.parseInt(splitImgName[1].substring(0,splitImgName[1].indexOf("M")));
                }
                Log.i("addimage splitimgname", splitImgName[1]);
                if (newImg.equals(file[i].getName())) {
                    fileExists = true;
                }
            }


            if (fileExists == true) {
                int large = largestAndSmallest(countImg);

                newImg = String.valueOf(large + 1);
            }
        }
        return newImg;
    }

    public int largestAndSmallest(int[] numbers) {
        int largest = Integer.MIN_VALUE;
        int smallest = Integer.MAX_VALUE;
        for (int number : numbers) {
            if (number > largest) {
                largest = number;
            } else if (number < smallest) {
                smallest = number;
            }
        }
        return largest;
    }

    private void setupRadioButtonsGroup() {
        for (int i = 0; i < mQuestionOptionsRadioGroup.getChildCount(); i++) {
            RadioButton radioButton = (RadioButton) mQuestionOptionsRadioGroup.getChildAt(i).findViewById(R.id.radio_button);
            setupRadioButton(radioButton);
        }
    }

    private void setupRadioButton(final RadioButton radioButton) {
        radioButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setAllRadioButtonsButOneUnchecked(radioButton);
            }
        });
    }

    private void setAllRadioButtonsButOneUnchecked(RadioButton checkedRadioButton) {
        for (int i = 0; i < mQuestionOptionsRadioGroup.getChildCount(); i++) {
            RadioButton radioButton = (RadioButton) mQuestionOptionsRadioGroup.getChildAt(i).findViewById(R.id.radio_button);
            if (checkedRadioButton.equals(radioButton)) {
                radioButton.setChecked(true);
            } else {
                radioButton.setChecked(false);
            }
        }
    }

    public void addOption(boolean shouldAllowDelete, boolean isChecked, String optionString) {

        // check if adding option is allowed
        if (mQuestionOptionsRadioGroup.getChildCount() >= MAX_OPTIONS_COUNT) {
            Toast.makeText(getContext(), "Cannot add more than " + MAX_OPTIONS_COUNT + " options", Toast.LENGTH_LONG).show();
            return;
        }

        mConditionSatisfiesStackList.add(!optionString.equals(""));
        final int index = mConditionSatisfiesStackList.size() - 1;

        final View singleOptionView = getLayoutInflater().inflate(R.layout.content_single_choice_question_option, null);

        // Instantiate UI Components
        final EditText editText = (EditText) singleOptionView.findViewById(R.id.edit_text);
        final RadioButton radioButton = (RadioButton) singleOptionView.findViewById(R.id.radio_button);
        final Button deleteButton = (Button) singleOptionView.findViewById(R.id.delete_button);

        editText.requestFocus();
        radioButton.setChecked(isChecked);
        editText.setText(optionString);

        // Set listeners
        editText.addTextChangedListener(new MinimumConditionOnTextChangeListener(this, editText, index));
        if (shouldAllowDelete) {
            deleteButton.setVisibility(View.VISIBLE);
            deleteButton.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {
                    mQuestionOptionsRadioGroup.removeView(singleOptionView);
                    mAddOptionImageButton.setVisibility(View.VISIBLE);

                    if (!MinimumConditionOnTextChangeListener.doesItReallyHaveChars(editText.getText().toString())) {
                        mConditionSatisfiesStackList.set(index, true);
                    }
                    calculateDoneButtonEnabled();
                }

            });
        } else {
            deleteButton.setVisibility(View.GONE);
        }
        setupRadioButton(radioButton);

        // add view to RadioGroup
        mQuestionOptionsRadioGroup.addView(singleOptionView);

        // if addOption button has to be disabled
        if (mQuestionOptionsRadioGroup.getChildCount() == MAX_OPTIONS_COUNT) {
            mAddOptionImageButton.setVisibility(View.GONE);
        }

        mDoneButton.setEnabled(false);
    }

    public void addOption(boolean shouldAllowDelete, boolean isChecked) {
        addOption(shouldAllowDelete, isChecked, "");
    }

    private void addOption(boolean shouldAllowDelete) {
        addOption(shouldAllowDelete, false);
    }

    private void addOption() {
        addOption(true);
    }

    private void loadStateFromQuestion() {
        // TODO Implement
    }

    @Override
    public void conditionSatisfied(int index) {
        mConditionSatisfiesStackList.set(index, true);
        calculateDoneButtonEnabled();
    }

    @Override
    public void conditionFailed(int index) {
        mConditionSatisfiesStackList.set(index, false);
        calculateDoneButtonEnabled();
    }

    public void calculateDoneButtonEnabled() {
        System.out.println(mConditionSatisfiesStackList);
        for (boolean b : mConditionSatisfiesStackList) {
            if (!b) {
                mDoneButton.setEnabled(false);
                return;
            }
        }
        mDoneButton.setEnabled(true);
    }

}
