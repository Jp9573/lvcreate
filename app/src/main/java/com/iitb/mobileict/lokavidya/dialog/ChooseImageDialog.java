package com.iitb.mobileict.lokavidya.dialog;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.AnyRes;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;

import com.darsh.multipleimageselect.activities.AlbumSelectActivity;
import com.darsh.multipleimageselect.helpers.Constants;
import com.iitb.mobileict.lokavidya.R;

import java.io.File;

import me.nereo.multi_image_selector.MultiImageSelectorActivity;

/**
 * Created by ironstein on 27/04/17.
 */

public class ChooseImageDialog extends Dialog {

    private Activity mActivity;
    private Fragment mFragment;
    private Context mContext;
    private Button mChooseGalleryImageButton;
    private Button mTakeCameraImageButton;
    //private boolean isItAFragment = true;

    static int CAM_PERMISSONS_REQUEST_CODE;
    public static final int REQUEST_IMAGE = 1; //just a constant
    public int flag = 0;

    public ChooseImageDialog(Context context, Fragment fragment) {
        super(context);
        mFragment = fragment;
        mActivity = fragment.getActivity();
        mContext = context;
    }

    /*public ChooseImageDialog(Context context) {
        super(context);
        mContext = context;
        mActivity = (Activity) mContext;
        isItAFragment = false;
    }*/

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Setup UI
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.content_choose_image_dialog);

        // instantiate UI Components
        mChooseGalleryImageButton = (Button) findViewById(R.id.button_choose_image_from_gallery);
        mTakeCameraImageButton = (Button) findViewById(R.id.button_take_camera_image);

        mChooseGalleryImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M
                        && ContextCompat.checkSelfPermission(mContext, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(mActivity, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
                    return;
                }

                Log.i("inside gallery method", "ok");

                Intent intent = new Intent(getContext(), MultiImageSelectorActivity.class);

                // whether show camera
                intent.putExtra(MultiImageSelectorActivity.EXTRA_SHOW_CAMERA, false);

                // max select image amount
                intent.putExtra(MultiImageSelectorActivity.EXTRA_SELECT_COUNT, 5);

                // select mode (MultiImageSelectorActivity.MODE_SINGLE OR MultiImageSelectorActivity.MODE_MULTI)
                intent.putExtra(MultiImageSelectorActivity.EXTRA_SELECT_MODE, MultiImageSelectorActivity.MODE_MULTI);

                mFragment.startActivityForResult(intent, REQUEST_IMAGE);

                dismiss();
            }
        });


        mTakeCameraImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, getPhotoFileUri("temp.png"));
                if (weHavePermissionToReadContacts()) {
                    mFragment.startActivityForResult(intent, 2);
                } else {

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        flag = 1;
                        mFragment.requestPermissions(new String[]{Manifest.permission.CAMERA}, CAM_PERMISSONS_REQUEST_CODE);
                        //startActivityForResult(intent, 2);
                    }
                }
                dismiss();
            }
        });

        /*mTakeCameraImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), CameraActivity.class);
                if (isItAFragment)
                    mFragment.startActivityForResult(intent, SharedRuntimeContent.GET_MULTIPLE_CAMERA_IMAGES);
                else
                    mActivity.startActivityForResult(intent, SharedRuntimeContent.GET_MULTIPLE_CAMERA_IMAGES);
                ChooseImageDialog.this.dismiss();
            }
        });*/
    }

    public Uri getPhotoFileUri(String fileName) {


        if (isExternalStorageAvailable()) {


            File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "lokavidya_images");

            if (!mediaStorageDir.exists() && !mediaStorageDir.mkdirs()) {

            }


            return Uri.fromFile(new File(mediaStorageDir.getPath() + File.separator + fileName));


        }
        return null;
    }

    private boolean isExternalStorageAvailable() {
        String state = Environment.getExternalStorageState();
        if (state.equals(Environment.MEDIA_MOUNTED)) {
            return true;
        }
        return false;
    }

    private boolean weHavePermissionToReadContacts() {

        return ContextCompat.checkSelfPermission(getContext(), Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED;

    }

    /**
     * get uri to any resource type
     *
     * @param context - context
     * @param resId   - resource id
     * @return - Uri to resource by given id
     * @throws Resources.NotFoundException if the given ID does not exist.
     */
    /*public static final Uri getUriFromResource(@NonNull Context context,
                                               @AnyRes int resId)
            throws Resources.NotFoundException {
        *//** Return a Resources instance for your application's package. *//*
        Resources res = context.getResources();
        *//**
     * Creates a Uri which parses the given encoded URI string.
     * @param uriString an RFC 2396-compliant, encoded URI
     * @throws NullPointerException if uriString is null
     * @return Uri for this given uri string
     *//*
        Uri resUri = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE +
                "://" + res.getResourcePackageName(resId)
                + '/' + res.getResourceTypeName(resId)
                + '/' + res.getResourceEntryName(resId));
        *//** return uri *//*
        return resUri;
    }*/

}
