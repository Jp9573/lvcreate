package com.iitb.mobileict.lokavidya.fragment;


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.iitb.mobileict.lokavidya.Projectfile;
import com.iitb.mobileict.lokavidya.R;
import com.iitb.mobileict.lokavidya.activity.WelcomeActivity;
import com.iitb.mobileict.lokavidya.util.Master;

import org.w3c.dom.Text;

import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileFragment extends Fragment {

    View view;

    public ProfileFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.profile_fragment, container, false);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        ConstraintLayout profileLayout, signinLayout;

        profileLayout = view.findViewById(R.id.profile_layout);
        signinLayout = view.findViewById(R.id.sign_in_layout);

        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getContext());
        String name = sharedPref.getString("UserName", "Guest");
        String phone = sharedPref.getString("UserPhone", "-");

        if(name.equals("Guest")) {
            profileLayout.setVisibility(View.GONE);
            signinLayout.setVisibility(View.VISIBLE);

            Button btnTakeMeThere = view.findViewById(R.id.btn_take_me_there);
            btnTakeMeThere.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent k= new Intent(getActivity(), WelcomeActivity.class);
                    startActivity(k);
                    getActivity().finishAffinity();
                }
            });

        }else {
            TextView tvName, tvPhone;
            tvName = view.findViewById(R.id.pf_tv_name);
            tvPhone = view.findViewById(R.id.pf_tv_phone);

            Button logout = view.findViewById(R.id.btn_logout);

            tvName.setText("User: " + name);
            tvPhone.setText("Phone: " + phone);

            ListView listView = view.findViewById(R.id.ProjectList);
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(), R.layout.listitem, projectsList());
            listView.setAdapter(adapter);
            adapter.notifyDataSetChanged();
            registerForContextMenu(listView); //for floating context menu (on long click)

            logout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final AlertDialog.Builder builder1 = new AlertDialog.Builder(getContext());
                    builder1.setMessage("Are you sure you want to logout?");
                    builder1.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getContext());
                            sharedPreferences.edit().remove("idToken").apply();
                            sharedPreferences.edit().remove("UserName").apply();
                            sharedPreferences.edit().remove("UserPhone").apply();

                            Intent k= new Intent(getActivity(), WelcomeActivity.class);
                            startActivity(k);
                            getActivity().finishAffinity();
                        }
                    }) .setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    });

                    builder1.create().show();
                }
            });

            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                public void onItemClick(AdapterView<?> adapter, View v, int position, long id) {
                    String projectName = (String) adapter.getItemAtPosition(position);
                    ChangeTab changeTab = (ChangeTab) getActivity();
                    changeTab.switchToProject(projectName);
                }
            });
        }
    }

    public List<String> projectsList() {
        Context context = getContext();
        Projectfile f = new Projectfile(context);
        List<String> myStringArray = f.DisplayProject();
        return myStringArray;
    }

    public interface ChangeTab {
        void switchToProject(String projectName);
    }

}
