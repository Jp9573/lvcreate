package com.iitb.mobileict.lokavidya.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.iitb.mobileict.lokavidya.R;
import com.iitb.mobileict.lokavidya.ui.Projects;

public class HomeFragment extends Fragment {

    View rootView;
    private ConstraintLayout createProjectRelativeLayout;
    private Button createProject;
    private ConstraintLayout viewVideosRelativeLayout;
    private Button viewVideos;

    public HomeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.home_fragment, container, false);
        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        createProject = (Button) rootView.findViewById(R.id.createProject);
        viewVideos = (Button) rootView.findViewById(R.id.viewVideos);
        createProjectRelativeLayout = rootView.findViewById(R.id.create_projects_relative_layout);
        viewVideosRelativeLayout = rootView.findViewById(R.id.view_videos_relative_layout);

        createProjectRelativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        createProject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            }
        });

        viewVideosRelativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            }
        });

        viewVideos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), Projects.class);
                startActivity(intent);
            }
        });

    }
}
