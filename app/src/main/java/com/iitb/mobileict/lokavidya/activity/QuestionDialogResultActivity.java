package com.iitb.mobileict.lokavidya.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class QuestionDialogResultActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setResult(RESULT_OK, getIntent());
        finish();
    }
}
