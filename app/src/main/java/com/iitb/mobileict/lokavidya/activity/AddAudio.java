package com.iitb.mobileict.lokavidya.activity;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Environment;
import android.os.Handler;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.iitb.mobileict.lokavidya.R;
import com.iitb.mobileict.lokavidya.ui.Projects;
import com.iitb.mobileict.lokavidya.util.Master;
import com.iitb.mobileict.lokavidya.util.Utilities;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;

public class AddAudio extends AppCompatActivity implements SeekBar.OnSeekBarChangeListener{

    public static int MY_REQUEST_CODE1;
    public String imagefileName, projectName;
    public int flag;
    SharedPreferences sharedPref;
    SharedPreferences.Editor editor;
    Button play, stop, record, retry, uploadAudio;
    boolean alreadyRecorded = false;
    boolean isRecording = false;
    int count = -1;
    File image_file;
    File tmp_file;
    Projects p;
    private SeekBar audioProgressBar;
    private MediaRecorder myAudioRecorder;
    private MediaPlayer m;
    private String outputFile = null;
    private ImageView imageView;
    private Handler mHandler = new Handler();
    private TextView songTotalDurationLabel;
    private TextView songCurrentDurationLabel;
    private Utilities utils;
    private Runnable mUpdateTimeTask = new Runnable() {
        public void run() {
            long totalDuration = m.getDuration();
            long currentDuration = m.getCurrentPosition();


            songTotalDurationLabel.setText("" + utils.milliSecondsToTimer(totalDuration));

            songCurrentDurationLabel.setText("" + utils.milliSecondsToTimer(currentDuration));


            int progress = (int) (utils.getProgressPercentage(currentDuration, totalDuration));

            audioProgressBar.setProgress(progress);
            if (!(m.isPlaying())) {
                mHandler.removeCallbacks(mUpdateTimeTask);
                return;
            }

            mHandler.postDelayed(this, 100);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_audio);

        setTitle("Add Audio");

        sharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        editor = sharedPref.edit();
        songCurrentDurationLabel = (TextView) findViewById(R.id.current);
        songTotalDurationLabel = (TextView) findViewById(R.id.total);
        audioProgressBar = (SeekBar) findViewById(R.id.seekBar);
        //recordProgressBar = (ProgressBar) findViewById(R.id.progBar);
        audioProgressBar.setOnSeekBarChangeListener(this);
        utils = new Utilities();
        Intent intent = getIntent();

        imagefileName = /*getAudioFileName(); */intent.getStringExtra("filename");
        projectName = Master.getCurrentProjectName();/*intent.getStringExtra("projectname");*/
        //extention = intent.getStringExtra("extention");

        final Chronometer myChronometer = (Chronometer) findViewById(R.id.chronometer);

        p = new Projects(); // for making use of isNetworkAvailable();

        count = sharedPref.getInt(projectName, 0);
        if (count == 0) {
            editor.putInt(projectName, 0);
            editor.commit();
        }

        image_file = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/lokavidya" + "/" + Master.getCurrentProjectName() + "/audio", imagefileName);
        //tmp_file = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/lokavidya" + "/" + projectName + "/tmp_images", imagefileName);

        //Bitmap myBitmap = BitmapFactory.decodeFile(image_file.getAbsolutePath());
        imageView = (ImageView) findViewById(R.id.imagePlaying);
        imageView.setImageResource(R.drawable.logo_circle);

        play = (Button) findViewById(R.id.pausePlaying);
        stop = (Button) findViewById(R.id.stopPlaying);
        record = (Button) findViewById(R.id.startPlaying);
        retry = (Button) findViewById(R.id.retryRecording);
        uploadAudio = findViewById(R.id.upload_audio);

        stop.setEnabled(false);
        play.setEnabled(false);
        retry.setEnabled(false);
        String path = null;
        try {
            path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/lokavidya" + "/" + projectName + "/audio/";
            outputFile = path + imagefileName + ".wav";
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

        File dir = new File(path);
        File file = new File(outputFile);

        if(file.exists()) {
            alreadyRecorded = true;
            play.setEnabled(true);
            retry.setEnabled(true);
            record.setEnabled(false);
        }else {
            play.setEnabled(false);
            retry.setEnabled(false);
            record.setEnabled(true);
        }

        if (!dir.exists()) {
            dir.mkdirs();
        }

        m = new MediaPlayer();
        myAudioRecorder = new MediaRecorder();
        myAudioRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        myAudioRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
        myAudioRecorder.setAudioEncoder(MediaRecorder.OutputFormat.AMR_NB);
        myAudioRecorder.setOutputFile(outputFile);

        uploadAudio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*Intent intent;
                intent = new Intent();
                intent.setAction(Intent.ACTION_GET_CONTENT);
                intent.setType("audio/mpeg");
                startActivityForResult(Intent.createChooser(intent, getString(R.string.select_audio_file_title)), 11);*/
                Intent intent = new Intent();
                intent.setType("audio/mpeg");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(
                        intent, "Open Audio (mp3) file"), 11);
            }
        });

        record.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(weHavePermissionToRecord()) {
                    audioProgressBar.setVisibility(View.GONE);
                    songCurrentDurationLabel.setVisibility(View.GONE);
                    songTotalDurationLabel.setVisibility(View.GONE);
                    myChronometer.setVisibility(View.VISIBLE);

                    try {
                        myAudioRecorder.prepare();
                        myAudioRecorder.start();
                        isRecording = true;

                        myChronometer.setBase(SystemClock.elapsedRealtime());
                        myChronometer.start();

                    } catch (IllegalStateException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    play.setEnabled(false);
                    record.setEnabled(false);
                    stop.setEnabled(true);
                    retry.setEnabled(false);
                }
            }
        });

        stop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("..........isPlaying" + m.isPlaying() + "...");
                if (m.isPlaying()) {
                    m.stop();

                    audioProgressBar.setProgress(0);
                    stop.setEnabled(false);
                    uploadAudio.setEnabled(true);
                    play.setEnabled(true);
                    retry.setEnabled(true);
                    record.setEnabled(false);
                } else if (isRecording == true) {
                    stop.setEnabled(false);
                    uploadAudio.setEnabled(true);
                    Handler delayHandler = new Handler();
                    delayHandler.postDelayed(new Runnable() {
                        public void run() {
                            myAudioRecorder.stop();
                            myAudioRecorder.release();
                            //recordProgressBar.setVisibility(View.GONE);
                            myChronometer.setVisibility(View.GONE);
                            audioProgressBar.setVisibility(View.VISIBLE);
                            songCurrentDurationLabel.setVisibility(View.VISIBLE);
                            songTotalDurationLabel.setVisibility(View.VISIBLE);
                            audioProgressBar.setProgress(0);
                            myAudioRecorder = null;
                            isRecording = false;
                            myChronometer.stop();
                            boolean flag = false;
                            boolean flag_jpeg = false;

                            editor.putInt("savedView", 0);
                            editor.commit();

                            for (int i = 0; i < sharedPref.getInt(projectName, 0); i++) {
                                if ((imagefileName + ".png").equals(sharedPref.getString(projectName + "image_name" + (i + 1), ""))) {
                                    flag = true;
                                    break;
                                } else if ((imagefileName + ".jpg").equals(sharedPref.getString(projectName + "image_name" + (i + 1), ""))) {
                                    flag_jpeg = true;
                                    break;
                                }

                            }

                            if (!flag) {
                                count++;
                                editor.putInt(projectName, count);
                                editor.putString(projectName + "image_name" + count, imagefileName + ".png");
                                editor.putString(projectName + "image_path" + count, image_file.getAbsolutePath());

                                editor.commit();

                            }
                            if (!flag_jpeg) {
                                count++;
                                editor.putInt(projectName, count);
                                editor.putString(projectName + "image_name" + count, imagefileName + ".jpg");
                                editor.putString(projectName + "image_path" + count, image_file.getAbsolutePath());

                                editor.commit();

                            }

                            stop.setEnabled(false);
                            play.setEnabled(true);
                            retry.setEnabled(true);
                            record.setEnabled(false);
                            songTotalDurationLabel.setText(R.string.totallength);
                            songCurrentDurationLabel.setText(R.string.current);
                        }
                    }, 1000);

                } else {
                    stop.setEnabled(false);
                    play.setEnabled(true);
                    retry.setEnabled(true);
                    record.setEnabled(false);
                    uploadAudio.setEnabled(true);
                }

            }
        });

        retry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //recordProgressBar.setVisibility(View.VISIBLE);
                myChronometer.setVisibility(View.VISIBLE);
                audioProgressBar.setVisibility(View.GONE);
                songCurrentDurationLabel.setVisibility(View.GONE);
                songTotalDurationLabel.setVisibility(View.GONE);
                myAudioRecorder = new MediaRecorder();
                myAudioRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
                myAudioRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
                myAudioRecorder.setAudioEncoder(MediaRecorder.OutputFormat.AMR_NB);
                myAudioRecorder.setOutputFile(outputFile);
                try {
                    myAudioRecorder.prepare();
                    myAudioRecorder.start();
                    myChronometer.setBase(SystemClock.elapsedRealtime());
                    myChronometer.start();
                    isRecording = true;
                } catch (IllegalStateException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                record.setEnabled(false);
                stop.setEnabled(true);
                play.setEnabled(false);
                retry.setEnabled(false);
                uploadAudio.setEnabled(false);
                songTotalDurationLabel.setText(R.string.totallength);
                songCurrentDurationLabel.setText(R.string.current);

            }
        });

        play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) throws IllegalArgumentException, SecurityException, IllegalStateException {
                //recordProgressBar.setVisibility(View.GONE);
                myChronometer.setVisibility(View.GONE);
                audioProgressBar.setVisibility(View.VISIBLE);
                songCurrentDurationLabel.setVisibility(View.VISIBLE);
                songTotalDurationLabel.setVisibility(View.VISIBLE);
                m = new MediaPlayer();
                try {
                    m.setDataSource(outputFile);
                } catch (IOException e) {
                    e.printStackTrace();
                }

                try {
                    m.prepare();
                    m.start();
                    audioProgressBar.setProgress(0);
                    audioProgressBar.setMax(100);
                    updateProgressBar();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                record.setEnabled(false);
                play.setEnabled(false);
                stop.setEnabled(true);
                retry.setEnabled(false);
                uploadAudio.setEnabled(false);

            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 11 && resultCode == Activity.RESULT_OK){
            if ((data != null) && (data.getData() != null)){
                Uri audioFileUri = data.getData();

                String path = getAudioPath(audioFileUri.getPath());
                File source = new File(path);
                File destination = new File(outputFile);
                if(source.exists()) {
                    try {
                        copy(source, destination);
                        play.setEnabled(true);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }else {
                    Toast.makeText(this, "Please select file from Local Storage!", Toast.LENGTH_LONG).show();
                }
            }
        }
    }

    private void copy(File source, File destination) throws IOException {

        FileChannel in = new FileInputStream(source).getChannel();
        FileChannel out = new FileOutputStream(destination).getChannel();

        try {
            in.transferTo(0, in.size(), out);
        } catch(Exception e){
            e.printStackTrace();
        } finally {
            if (in != null)
                in.close();
            if (out != null)
                out.close();
        }
    }

    private String getAudioPath(String data) {
        String path = data.substring(data.indexOf(":") + 1);

        String fullpath = Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator + path;

        return fullpath;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == MY_REQUEST_CODE1) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {

            } else {
                Toast.makeText(getBaseContext(), "Cannot Open.", Toast.LENGTH_LONG).show();
            }
        }
    }

    private boolean weHavePermissionToRecord() {
        if ((ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO) == PackageManager.PERMISSION_GRANTED)) {
            return true;
        } else return false;
    }

    public void updateProgressBar() {
        mHandler.postDelayed(mUpdateTimeTask, 100);
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromTouch) {
        if (fromTouch)
            m.seekTo(progress);
    }


    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

        mHandler.removeCallbacks(mUpdateTimeTask);
    }


    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        mHandler.removeCallbacks(mUpdateTimeTask);
        int totalDuration = m.getDuration();
        int currentPosition = utils.progressToTimer(seekBar.getProgress(), totalDuration);


        m.seekTo(currentPosition);


        updateProgressBar();
    }

    public String getAudioFileName() {
        String newImg;
        File imgDir = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + "lokavidya" + "/" + Master.getCurrentProjectName() + "/images");

        if (!imgDir.exists()) {
            imgDir.mkdirs();
        }

        File file[] = imgDir.listFiles();
        newImg = Master.getCurrentProjectName() + "." + String.valueOf(file.length + 1) + ".wav";

        //newImg = Master.getCurrentProjectName() + "." + file[file.length - 1].getName().split("\\.")[1] + ".wav";

        if (file.length != 0) {
            boolean fileExists = false;

            int countImg[] = new int[file.length];

            for (int i = 0; i < file.length; i++) {
                String imgName = file[i].getName();
                Log.i("addimage imgname", imgName);
                imgName = imgName.replace(".mp4", "");
                Log.i("addimage imgname 2", imgName);
                String splitImgName[] = imgName.split("\\.");
                countImg[i] = Integer.parseInt(splitImgName[1]);
                Log.i("addimage splitimgname", splitImgName[1]);
                if (newImg.equals(file[i].getName())) {
                    fileExists = true;
                }
            }


            if (fileExists == true) {
                int large = largestAndSmallest(countImg);

                newImg = Master.getCurrentProjectName() + "." + String.valueOf(large + 1) + ".wav";
            }
        }
        return newImg;
    }

    public int largestAndSmallest(int[] numbers) {
        int largest = Integer.MIN_VALUE;
        int smallest = Integer.MAX_VALUE;
        for (int number : numbers) {
            if (number > largest) {
                largest = number;
            } else if (number < smallest) {
                smallest = number;
            }
        }
        return largest;
    }

}
