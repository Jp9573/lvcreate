package com.iitb.mobileict.lokavidya.interfaces;

/**
 * Created by jay on 6/3/18.
 */

public interface OnDone {
    public void done(boolean isDone);
}
