package com.iitb.mobileict.lokavidya.volley;


import com.iitb.mobileict.lokavidya.network.NetworkException;

public interface RequestResponseListener {

    interface Listener{
        <T> void onResponse(T response);
    }

    interface ErrorListener{
        void onError(NetworkException error);
    }

}
