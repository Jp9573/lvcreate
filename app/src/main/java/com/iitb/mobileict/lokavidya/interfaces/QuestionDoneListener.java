package com.iitb.mobileict.lokavidya.interfaces;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;
import com.iitb.mobileict.lokavidya.activity.QuestionDialogResultActivity;
import java.util.ArrayList;

public class QuestionDoneListener {

    public static String SLIDE_NUMBER_FIELD = "slide_number";

    private Context mContext;
    private Fragment mFragment = null;
    private Activity mActivity = null;

    public QuestionDoneListener(Context context, Fragment fragment) {
        mContext = context;
        mFragment = fragment;
    }

    public QuestionDoneListener(Context context, Activity activity) {
        mContext = context;
        mActivity = activity;
    }

    public void onDone(String questionString, ArrayList<String> options, ArrayList<Integer> correctOptions, int slideNumber) {
        Intent intent = new Intent(mContext, QuestionDialogResultActivity.class);
        intent.putExtra("question_string", questionString);
        intent.putExtra("options", options);
        intent.putExtra("correct_options", correctOptions);
        intent.putExtra("type", "mcq");
        intent.putExtra("solution", "");
        intent.putExtra("hints", new ArrayList<String>());
        intent.putExtra("is_compulsory", true);
        intent.putExtra(SLIDE_NUMBER_FIELD, slideNumber);

        if(mFragment != null) {
            mFragment.startActivityForResult(intent, 544);
        } else {
            mActivity.startActivityForResult(intent, 544);
        }
    }

}
