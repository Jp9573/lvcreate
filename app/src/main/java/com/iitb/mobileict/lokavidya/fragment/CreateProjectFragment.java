package com.iitb.mobileict.lokavidya.fragment;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Point;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.InputType;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

/*import com.github.hiteshsondhi88.libffmpeg.ExecuteBinaryResponseHandler;
import com.github.hiteshsondhi88.libffmpeg.FFmpeg;
import com.github.hiteshsondhi88.libffmpeg.LoadBinaryResponseHandler;
import com.github.hiteshsondhi88.libffmpeg.exceptions.FFmpegCommandAlreadyRunningException;
import com.github.hiteshsondhi88.libffmpeg.exceptions.FFmpegNotSupportedException;*/
import com.github.hiteshsondhi88.libffmpeg.ExecuteBinaryResponseHandler;
import com.github.hiteshsondhi88.libffmpeg.FFmpeg;
import com.github.hiteshsondhi88.libffmpeg.LoadBinaryResponseHandler;
import com.github.hiteshsondhi88.libffmpeg.exceptions.FFmpegCommandAlreadyRunningException;
import com.github.hiteshsondhi88.libffmpeg.exceptions.FFmpegNotSupportedException;
import com.iitb.mobileict.lokavidya.Projectfile;
import com.iitb.mobileict.lokavidya.R;
import com.iitb.mobileict.lokavidya.activity.AddAudio;
import com.iitb.mobileict.lokavidya.activity.DashboardActivity;
import com.iitb.mobileict.lokavidya.activity.TrimmerActivity;
import com.iitb.mobileict.lokavidya.activity.WatchVideoActivity;
import com.iitb.mobileict.lokavidya.adapter.SlidesRecyclerViewAdapter;
import com.iitb.mobileict.lokavidya.dialog.ChooseImageDialog;
import com.iitb.mobileict.lokavidya.dialog.SingleChoiceQuestionDialog;
import com.iitb.mobileict.lokavidya.ui.Recording;
import com.iitb.mobileict.lokavidya.ui.ViewVideo;
import com.iitb.mobileict.lokavidya.ui.shotview.GalleryItem;
import com.iitb.mobileict.lokavidya.util.Master;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.inject.Inject;

/*import me.nereo.multi_image_selector.MultiImageSelectorActivity;
import nl.bravobit.ffmpeg.ExecuteBinaryResponseHandler;
import nl.bravobit.ffmpeg.FFmpeg;
import nl.bravobit.ffmpeg.exceptions.FFmpegCommandAlreadyRunningException;*/

import me.nereo.multi_image_selector.MultiImageSelectorActivity;

import static android.app.Activity.RESULT_OK;

/**
 * Created by saifur on 16/10/15.
 */

/**
 * Implementation of the Editing activity- contains all the supporting methods
 */
public class CreateProjectFragment extends Fragment implements SlidesRecyclerViewAdapter.ClickCallback, DashboardActivity.PopulateData {
    public int flag = 0;
    String projectName = "";
    public static int RESIZE_FACTOR; // resize factor used for compression
    public static final int REQUEST_IMAGE = 1; //just a constant
    int count = 0;
    static int CAM_PERMISSONS_REQUEST_CODE;
    static int RECORDING_PERMISSIONS_REQUEST_CODE = 1;
    private static final int REQUEST_VIDEO_TRIMMER = 11;
    static final String EXTRA_VIDEO_PATH = "EXTRA_VIDEO_PATH";
    private int gridPosition;

    private Button mImagePicker;
    private Button mVideoPicker;
    private Button mAudioPicker;
    private Button mQuestionPicker;
    private TextView mProjectNameDisplay;
    FloatingActionButton fabNext;
    RecyclerView gridview;
    SlidesRecyclerViewAdapter adapter;
    ArrayList<GalleryItem> list;

    Projectfile f = null;
    View view;

    ArrayList<String[]> commandArrayList = new ArrayList<>();
    int counter = 0;
    private boolean isReadyForMerge = false;
    private boolean isVideoCompiled = false;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.create_project_fragment, container, false);
        System.out.println("---------- CreateProjectFragment onCreateView -------------");
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        System.out.println("---------- CreateProjectFragment onActivityCreated -------------");
        gridview = view.findViewById(R.id.gridview);
        list = new ArrayList<>();

        mImagePicker = (Button) view.findViewById(R.id.image_picker);
        mAudioPicker = (Button) view.findViewById(R.id.camera_image_picker);
        mVideoPicker = (Button) view.findViewById(R.id.video_picker);
        mQuestionPicker = (Button) view.findViewById(R.id.question_picker);
        mProjectNameDisplay = (TextView) view.findViewById(R.id.project_name_display);

        fabNext = view.findViewById(R.id.fab_next);

        //if(this.isResumed()) {

            /*if (projectName.equals("")) {
                showAddProjectDialog(projectName);
            } else {
                mProjectNameDisplay.setText(projectName);
                Master.setCurrentProjectName(projectName);
            }*/

        gridview.setLayoutManager(new GridLayoutManager(view.getContext(), 3));
        gridview.setHasFixedSize(true);
        adapter = new SlidesRecyclerViewAdapter(getContext(), this, f);
        gridview.setAdapter(adapter);

        //}

        mProjectNameDisplay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showAddProjectDialog(projectName);
            }
        });

        mImagePicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (f != null) {
                    ChooseImageDialog dialog = new ChooseImageDialog(getActivity(), CreateProjectFragment.this);
                    dialog.show();
                } else {
                    showAddProjectDialog(projectName);
                }
            }
        });

        mAudioPicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (f != null) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M
                            && ContextCompat.checkSelfPermission(getContext(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, 1);
                        return;
                    }
                    addAudio();
                } else {
                    showAddProjectDialog(projectName);
                }
            }
        });

        mVideoPicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (f != null) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M
                            && ContextCompat.checkSelfPermission(getContext(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, 1);
                        return;
                    }
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        getActivity().requestPermissions(new String[]{Manifest.permission.CAMERA}, CAM_PERMISSONS_REQUEST_CODE);
                        //startActivityForResult(intent, 2);
                    }
                    /*VideoPickerActivity dialog = new VideoPickerActivity(getActivity(), CreateProjectFragment.this);
                    dialog.show();*/
                    if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                        requestPermission(Manifest.permission.READ_EXTERNAL_STORAGE, getActivity().getString(R.string.permission_read_storage_rationale), 101);
                    } else {
                        Intent intent = new Intent();
                        intent.setTypeAndNormalize("video/*");
                        intent.setAction(Intent.ACTION_GET_CONTENT);
                        intent.addCategory(Intent.CATEGORY_OPENABLE);
                        startActivityForResult(Intent.createChooser(intent, getString(R.string.label_select_video)), REQUEST_VIDEO_TRIMMER);
                    }
                } else {
                    showAddProjectDialog(projectName);
                }
                /*Intent intent = new Intent();
                intent.setType("video*//*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Video"), GET_VIDEO);*/
            }
        });

        mQuestionPicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (f != null) {
                    SingleChoiceQuestionDialog dialog = new SingleChoiceQuestionDialog(getContext(), CreateProjectFragment.this);
                    dialog.show();
                    Window window = dialog.getWindow();
                    window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                } else {
                    showAddProjectDialog(projectName);
                }
            }
        });

        fabNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Master.imageNames.size() > 0) {
                    /*Intent intent1 = new Intent(getContext(), ViewShots.class);
                    intent1.putExtra("projectname", projectName);
                    startActivity(intent1);*/

                    /*Intent i = new Intent(getContext(), ViewVideo.class);
                    startActivity(i);*/

                    ffmpeg = FFmpeg.getInstance(getContext());
                    try {
                        ffmpeg.loadBinary(new LoadBinaryResponseHandler() {

                            @Override
                            public void onStart() {}

                            @Override
                            public void onFailure() {}

                            @Override
                            public void onSuccess() {}

                            @Override
                            public void onFinish() {}
                        });
                    } catch (FFmpegNotSupportedException e) {
                        // Handle if FFmpeg is not supported by device
                    }
                    //loadFFMpegBinary();
                    final String imagepath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/lokavidya" + "/" + Master.getCurrentProjectName() + "/images/";
                    final String audioPath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/lokavidya" + "/" + Master.getCurrentProjectName() + "/audio/";
                    final String path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/lokavidya" + "/" + Master.getCurrentProjectName();

                    commandArrayList.clear();
                    Master.videosNames.clear();
                    counter = 0;

                    File file = new File(path, "videos");
                    if(!file.exists()) {
                        file.mkdir();
                    }else {
                        deleteRecursive(file);
                        file.mkdir();
                    }

                    for (String imageFile : Master.imageNames) {
                        if(Master.audioNames.contains(imageFile + ".wav")) {
                            makeVideoFromImageAndAudio(imagepath, audioPath, imageFile, imageFile + ".wav");
                        }else if (imageFile.contains("mp4")) {
                            makeVideoFromVideo(imagepath, imageFile);
                        }else {
                            makeVideoFromSingleImage(imagepath, imageFile);
                        }
                    }

                    counter++;
                    execFFmpegBinary(commandArrayList.get(0));
                    //mergeAllVideos();


                } else
                    Toast.makeText(getContext(), "Empty project!!!", Toast.LENGTH_LONG).show();
            }
        });

    }

    void makeVideoFromSingleImage(String imagePath, String imageFileName) {
        String[] pices = imageFileName.split("\\.");
        String videoFileName = pices[0] + "." + pices[1] + ".mp4";
        String image = imagePath + imageFileName;

        String[] complexCommand =
                new String[]{
                        "-loop",
                        "1",
                        "-i",
                        image + "",
                        "-c:v",
                        "libx264",
                            "-preset",
                            "ultrafast",
                        "-t",
                        "3",
                        "-pix_fmt",
                        "yuv420p",
                        "-y",
                        "/storage/emulated/0/" + "lokavidya/" + Master.getCurrentProjectName() + "/videos/" + videoFileName};

        commandArrayList.add(complexCommand);
    }

    void makeVideoFromVideo(String videoPath, String videoFileName) {
        String[] pices = videoFileName.split("\\.");
        String outputVideoFileName = pices[0] + "." + pices[1] + ".mp4";
        String video = videoPath + videoFileName;

        String[] complexCommand =
                new String[] {
                        "-i",
                        video + "",
                        "-f",
                        "mp4",
                        "-vcodec",
                        "libx264",
                        "-preset",
                        "ultrafast",
                        "-profile:v",
                        "main",
                        "-acodec",
                        "aac",
                        "-y",
                        "/storage/emulated/0/" + "lokavidya/" + Master.getCurrentProjectName() + "/videos/" + outputVideoFileName,
                        "-hide_banner"};

        commandArrayList.add(complexCommand);
    }

    void makeVideoFromImageAndAudio(String imagePath, String audioPath, String imageFileName, String audioFileName) {
        String[] pices = imageFileName.split("\\.");
        String videoFileName = pices[0] + "." + pices[1] + ".mp4";

        String[] complexCommand =
                new String[]{
                    "-i",
                    imagePath + imageFileName,
                    "-i",
                    audioPath + audioFileName,
                    "-c:v",
                    "libx264",
                        "-preset",
                        "ultrafast",
                    "-c:a",
                    "aac",
                    "-pix_fmt",
                    "yuv420p",
                    "-y",
                    "/storage/emulated/0/" + "lokavidya/" + Master.getCurrentProjectName() + "/videos/" + videoFileName};
        commandArrayList.add(complexCommand);
    }

    void deleteRecursive(File fileOrDirectory) {
        if (fileOrDirectory.isDirectory())
            for (File child : fileOrDirectory.listFiles())
                deleteRecursive(child);

        fileOrDirectory.delete();
    }

    /*public void concatenate(String outputFile) {
        String list = generateList();
        execFFmpegBinary(new String[] {
                "-f",
                "concat",
                "-safe",
                "0",
                "-i",
                list,
                "-c",
                "copy",
                outputFile
        });
    }

    private String generateList() {
        File list;
        Writer writer = null;
        Master.videosNames = f.getVideoNames(Master.getCurrentProjectName());
        List<String> inputs = Master.videosNames;
        final String path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/lokavidya" + "/" + Master.getCurrentProjectName() + "/videos/";

        try {
            list = File.createTempFile("ffmpeg-list", ".txt");
            writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(list)));
            for (String input: inputs) {
                writer.write("file '" + path + input + "'\n");
                System.out.println("Writing to list file: file '" + input + "'");
            }
        } catch (IOException e) {
            e.printStackTrace();
            return "/";
        } finally {
            try {
                if (writer != null)
                    writer.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }

        System.out.println("Wrote list file to " + list.getAbsolutePath());
        return list.getAbsolutePath();
    }*/

    void mergeAllVideos() {
        //concatenate("/storage/emulated/0/" + "lokavidya/" + Master.getCurrentProjectName() + "/" + "Video.mp4");
        final String path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/lokavidya" + "/" + Master.getCurrentProjectName();

        File file = new File(path, "videos");
        File textFile = null;

        Master.videosNames = f.getVideoNames(Master.getCurrentProjectName());

        try {
            textFile = new File(file, "video-list.txt");
            FileWriter writer = new FileWriter(textFile);
            for (String fileName : Master.videosNames) {
                writer.append("file '" + fileName + "'\n");
            }
            writer.flush();
            writer.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        //Bettter than Original one!
        String[] complexCommand =
                new String[]{
                        "-f",
                        "concat",
                        "-i", //for specifying input
                        textFile + "",
                            "-preset",
                            "ultrafast",
                        "-fflags",
                        "+genpts",
                        "-y", //Overwrite output files without asking.
                        "/storage/emulated/0/" + "lokavidya/" + Master.getCurrentProjectName() + "/" + "Video.mp4"};

        //Original
        /*String[] complexCommand =
            new String[]{
                "-f",
                "concat",
                "-i", //for specifying input
                textFile + "",
                "-c",
                "copy",
                "-y", //Overwrite output files without asking.
                "/storage/emulated/0/" + "lokavidya/" + Master.getCurrentProjectName() + "/" + "Video.mp4"};*/
        execFFmpegBinary(complexCommand);

        /*Intent intent = new Intent(getActivity(), WatchVideoActivity.class);
        startActivity(intent);*/

    }

    @Inject
    FFmpeg ffmpeg;

    private void execFFmpegBinary(final String[] command) {
        try {
            ffmpeg.execute(command, new ExecuteBinaryResponseHandler() {
                @Override
                public void onFailure(String s) {
                    //addTextViewToLayout("FAILED with output : "+s);
                    Toast.makeText(getContext(), "Fail!" + s, Toast.LENGTH_LONG).show();
                    System.out.println("------err--------" + s);
                }

                @Override
                public void onSuccess(String s) {
                    //addTextViewToLayout("SUCCESS with output : "+s);
                    if(counter <= commandArrayList.size())
                        Toast.makeText(getContext(), "File " + counter + " successfully compiled!", Toast.LENGTH_LONG).show();

                    if(counter > commandArrayList.size())
                        isVideoCompiled = true;
                }

                @Override
                public void onProgress(String s) {
                    //addTextViewToLayout("progress : "+s);
                    System.out.println("progress : "+s);
                    Master.setProgress(s);
                }

                @Override
                public void onStart() {
                    //outputLayout.removeAllViews();
                    Master.showProgressDialog(getContext(), "Processing...");
                }

                @Override
                public void onFinish() {
                    Master.dismissProgressDialog();
                    if(counter < commandArrayList.size()) {
                        if(counter == commandArrayList.size() - 1) {
                            isReadyForMerge = true;
                        }
                        counter++;
                        execFFmpegBinary(commandArrayList.get(counter - 1));
                    }else if(counter == commandArrayList.size()){
                        isReadyForMerge = true;
                        counter++;
                    }

                    if(isReadyForMerge){
                        isReadyForMerge = false;
                        mergeAllVideos();
                    }

                    if(isVideoCompiled) {
                        isVideoCompiled = false;
                        Intent intent = new Intent(getActivity(), WatchVideoActivity.class);
                        startActivity(intent);
                    }
                }
            });
        } catch (FFmpegCommandAlreadyRunningException e) {
            e.printStackTrace();
        }
    }

    private void requestPermission(final String permission, String rationale, final int requestCode) {
        if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), permission)) {
            android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(getContext());
            builder.setTitle(getContext().getString(R.string.permission_title_rationale));
            builder.setMessage(rationale);
            builder.setPositiveButton(getContext().getString(R.string.label_ok), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    ActivityCompat.requestPermissions(getActivity(), new String[]{permission}, requestCode);
                }
            });
            builder.setNegativeButton(getContext().getString(R.string.label_cancel), null);
            builder.show();
        } else {
            ActivityCompat.requestPermissions(getActivity(), new String[]{permission}, requestCode);
        }
    }

    public void addProject(String newproject) {
        if (newproject.equals("") || newproject.equals(" "))
            return; //(Sanket P) changed newproject == "" to newproject.equals("").
        f = new Projectfile(getContext());
        Master.setCurrentProjectName(newproject);
        mProjectNameDisplay.setText(newproject);
        loadImages(true);
    }

    public void showAddProjectDialog(String projectName) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle(R.string.enterName);

        final EditText input = new EditText(getContext());

        input.setText(projectName);
        input.requestFocus();
        input.setInputType(InputType.TYPE_CLASS_TEXT);
        builder.setView(input);

        builder.setPositiveButton(getString(R.string.OkButton), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                //all the checks and validations for an appropriate project names are performed:

                Pattern pattern1 = Pattern.compile("\\s");
                Pattern pattern2 = Pattern.compile("\\.");
                // Pattern pattern3 = Pattern.compile("");

                //Matcher matcher1 = pattern1.matcher(input.getText().toString());
                Matcher matcher2 = pattern2.matcher(input.getText().toString());
                // Matcher matcher3 = pattern3.matcher(input.getText().toString());

                //boolean found1 = matcher1.find();
                boolean found1 = false;
                boolean found2 = matcher2.find();
                boolean found3 = input.getText().toString().contains("/");
                // boolean found3 = matcher3.find();

                if (input.getText().toString().equals("")) {
                    Toast.makeText(getContext(), getString(R.string.projectNameEmpty), Toast.LENGTH_LONG).show();
                } else if (input.getText().toString().charAt(0) == ' ' || input.getText().toString().endsWith(" "))
                    found1 = true;
                if (found1)
                    Toast.makeText(getContext(), getString(R.string.projectNameSpace), Toast.LENGTH_LONG).show();
                else if (found2)
                    Toast.makeText(getContext(), getString(R.string.projectNameDot), Toast.LENGTH_LONG).show();
                else if (found3)
                    Toast.makeText(getContext(), "Project name cannot contain '/'", Toast.LENGTH_LONG).show();
                else {
                    addProject(input.getText().toString());
                }
            }
        });
        builder.setNegativeButton(getString(R.string.CancelButton), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();
    }

    void addAudio() {
        Bitmap icon = BitmapFactory.decodeResource(getContext().getResources(), R.drawable.logo_circle);

        f.addImage(icon);

        loadImages(true);

        Intent intent = new Intent(getActivity(), AddAudio.class);

        intent.putExtra("filename", Master.imageNames.get(Master.imageNames.size() - 1));
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.RECORD_AUDIO) == PackageManager.PERMISSION_GRANTED) {
            startActivity(intent);
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                flag = 1;
                requestPermissions(new String[]{Manifest.permission.RECORD_AUDIO}, RECORDING_PERMISSIONS_REQUEST_CODE);
            }
        }
    }

    @Override
    public void onStart() {
        super.onStart();

        System.out.println("---------- CreateProjectFragment OnStart -------------");

        loadImages(true);  //goes to the project folder and loads all the images in the gridview
    }

    @Override
    public void onResume() {
        super.onResume();
        System.out.println("---------- CreateProjectFragment OnStart -------------");

    }

    /*@Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_edit_project, menu);
        return true;
    }*/

    /*@Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();


        return id == R.id.action_settings || super.onOptionsItemSelected(item);

    }*/

    public Bitmap getImage(String path) throws IOException {
        ContentResolver cr = getActivity().getContentResolver();
        Bitmap bitmap = MediaStore.Images.Media.getBitmap(cr, getImageContentUri(getActivity(), path));
        return bitmap;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        RESIZE_FACTOR = getScreenwidth();

        switch (requestCode) {
            case REQUEST_IMAGE: //when you tap on 'Choose from gallery'
                if (resultCode == RESULT_OK) {
                    //Uri imageUri = imageReturnedIntent.getData();
                    final List<String> path = data.getStringArrayListExtra(MultiImageSelectorActivity.EXTRA_RESULT);
                    Log.i("wth inside gallery case", path.get(0));

                    //final ProgressDialog imageLoadingProgress = ProgressDialog.show(getContext(), getString(R.string.stitchingProcessTitle), getString(R.string.galleryImageProcessMessage), true);
                    Master.showProgressDialog(getActivity(), getString(R.string.galleryImageProcessMessage));

                    /*imageLoadingProgress.setCancelable(false);
                    imageLoadingProgress.setCanceledOnTouchOutside(false);*/
                    final ContentResolver cr = getActivity().getContentResolver();

                    new Thread(new Runnable() {
                        @TargetApi(Build.VERSION_CODES.M)
                        @Override
                        public void run() {

                            try {
                                int i;
                                for (i = 0; i < path.size(); i++) {
                                    Log.i("image content URI", getImageContentUri(getContext(), path.get(i)).toString());
                                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(cr, getImageContentUri(getContext(), path.get(i))); //getbitmap() needs content uri as its parameter. for that see getimage content uri() method.
                                    bitmap = getResizedBitmap(bitmap, RESIZE_FACTOR);

                                    /*if(f == null)
                                        showAddProjectDialog(projectName);*/

                                    f.addImage(bitmap);

                                    System.out.println("...........................??==" + bitmap.getHeight());
                                    System.out.println("...........................??==" + bitmap.getWidth());
                                }

                            } catch (IOException fe) {
                                toast("Image file not found in the library " + Uri.parse(path.get(0)));
                            }

                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    loadImages(true);
                                    //imageLoadingProgress.dismiss();
                                    Master.dismissProgressDialog();
                                }
                            });
                        }
                    }).start();


                }
                break;
            case 2: // when 'take a photo' is pressed

                if (resultCode == RESULT_OK) {
                    Uri takenPhotoUri = getPhotoFileUri("temp.png");
                    if (takenPhotoUri != null) {
                        Log.i("inside onActvtyRslt cam", takenPhotoUri.toString());

                        Bitmap photo = BitmapFactory.decodeFile(takenPhotoUri.getPath());
                        photo = getResizedBitmap(photo, RESIZE_FACTOR);

                        System.out.println("...........................??" + photo.getHeight());
                        System.out.println("...........................??" + photo.getWidth());

                    /*if(f == null)
                        showAddProjectDialog(projectName);*/

                        f.addImage(photo);

                        loadImages(true);
                    }
                } else {
                    Toast.makeText(getActivity(), "Picture wasn't taken!", Toast.LENGTH_SHORT).show();
                }
                break;

            case REQUEST_VIDEO_TRIMMER: // GET VIDEO FROM GALLERY
                if (data != null) {
                    final Uri selectedUri = data.getData();
                    if (selectedUri != null) {
                        try {
                            Intent intent = new Intent(getActivity(), TrimmerActivity.class);
                            intent.putExtra(EXTRA_VIDEO_PATH, life.knowledge4.videotrimmer.utils.FileUtils.getPath(getActivity(), selectedUri));
                            startActivity(intent);
                        } catch (Exception e) {
                            Toast.makeText(getActivity(), "Please select Video from Local storage!", Toast.LENGTH_LONG).show();
                        }
                    } else {
                        Toast.makeText(getActivity(), R.string.toast_cannot_retrieve_selected_video, Toast.LENGTH_SHORT).show();
                    }
                }
                break;

            case 544:
                loadImages(true);
                break;
            default:
                break;
        }
    }

    /**
     * make a toast!
     *
     * @param text string to put in the toast
     */
    public void toast(String text) {
        int duration = Toast.LENGTH_SHORT;
        Toast toast = Toast.makeText(getActivity(), text, duration);
        toast.show();
    }

    public int getScreenwidth() {
        Display display = getActivity().getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        return width;
    }

    public static Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        Log.i("getresizedbitmapInitial", "width: " + Integer.toString(width) + " height: " + Integer.toString(height));
        float bitmapRatio = (float) width / (float) height;

        if (bitmapRatio >= 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        Log.i("getresizedbitmapChanged", "width: " + Integer.toString(width) + " height: " + Integer.toString(height));


        //return Bitmap.createScaledBitmap(image, width, height, true);

        return addpaddingBitmap(image, width, height, maxSize);
//        return Bitmap.createScaledBitmap(image, width, height, true);

    }

    /**
     * opens the recording activity of the given image in the project.
     *
     * @param position position of the image in the gridview
     */
    @Override
    public void ImageClickCallBack(int position) {
        Intent intent = new Intent(getActivity(), Recording.class);

        intent.putExtra("filename", Master.imageNames.get(position));
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.RECORD_AUDIO) == PackageManager.PERMISSION_GRANTED) {

            startActivity(intent);
        } else {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                flag = 1;
                requestPermissions(new String[]{Manifest.permission.RECORD_AUDIO},
                        RECORDING_PERMISSIONS_REQUEST_CODE);
            }

        }

    }

    @SuppressLint("LongLogTag")
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        for (int i : grantResults) {
            Log.i("onrequestpermissionsresult", "array:" + i);
        }
        for (String s : permissions) {
            Log.i("onrequestpermissionsresult", "String:" + s);

        }
        if (requestCode == CAM_PERMISSONS_REQUEST_CODE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                if (flag == 1) {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, getPhotoFileUri("temp.png"));
                    startActivityForResult(intent, 2);
                }
            } else {
                Toast.makeText(getActivity(), "Cannot open Camera", Toast.LENGTH_LONG).show();
            }
        } else if (requestCode == RECORDING_PERMISSIONS_REQUEST_CODE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                if (flag == 1) {
                    Intent intent = new Intent(getActivity(), Recording.class);
                    intent.putExtra("projectname", projectName);
                    Projectfile f = new Projectfile(getActivity());
                    List<String> ImageNames = f.getImageNames(projectName);
                    String imagefilename = ImageNames.get(gridPosition);


                    imagefilename = imagefilename.substring(0, imagefilename.length() - 4);
                    String extention = imagefilename.substring(imagefilename.length() - 3, imagefilename.length());
                    /* System.out.println("extention -------------------" + extention + "");*/
                    intent.putExtra("filename", imagefilename);
                    intent.putExtra("extention", extention);
                    startActivity(intent);

                } else {
                    Toast.makeText(getActivity(), "Cannot perform this activity", Toast.LENGTH_LONG).show();
                }

            } else {
                Toast.makeText(getActivity(), "Cannot perform this activity", Toast.LENGTH_LONG).show();
            }
        } else {
            Toast.makeText(getActivity(), "Cannot perform this activity", Toast.LENGTH_LONG).show();
        }
    }

    /**
     * returns the uri of the given file path
     *
     * @param fileName the image filename
     * @return
     */
    public Uri getPhotoFileUri(String fileName) {


        if (isExternalStorageAvailable()) {


            File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "lokavidya_images");

            if (!mediaStorageDir.exists() && !mediaStorageDir.mkdirs()) {

            }


            return Uri.fromFile(new File(mediaStorageDir.getPath() + File.separator + fileName));


        }
        return null;
    }

    /**
     * check if SD card is available
     *
     * @return true if available, false if not
     */
    private boolean isExternalStorageAvailable() {
        String state = Environment.getExternalStorageState();
        if (state.equals(Environment.MEDIA_MOUNTED)) {
            return true;
        }
        return false;
    }

    /**
     * Loads the images and displays them into the  gridview
     */
    public void loadImages(boolean isNew) {
        //Projectfile f = new Projectfile(getActivity());
        if (f != null) {
            List<String> ImageNames = f.getImageNames(projectName);

            ArrayList<GalleryItem> galleryItemsList = new ArrayList<GalleryItem>();
            String imagefilename;
            //= ImageNames.get(position);

            File sdCard = Environment.getExternalStorageDirectory();

            Bitmap myBitmap;

            File imgDir = new File(sdCard.getAbsolutePath() + "/lokavidya" + "/" + projectName + "/images");
            File image_file;
            for (int i = 0; i < ImageNames.size(); i++) {
                imagefilename = ImageNames.get(i);
                // image_file=  new File(imgDir, imagefilename);
                //myBitmap = BitmapFactory.decodeFile(image_file.getAbsolutePath());
                //galleryItemsList.add(new GalleryItem(myBitmap,i,false));
                galleryItemsList.add(new GalleryItem(imagefilename, i, false));
                list.add(new GalleryItem(imagefilename, i, false));
            }

            for (int i = 0; i < galleryItemsList.size(); i++)
                System.out.println("loading image pos------->" + galleryItemsList.get(i).position + "---------" + galleryItemsList.get(i).imgFileName);

            if (isNew) {//this check was added to decide whether to add the images in the gridview, since if you need to call it from other method, this UI cannot be updated
                //adapter.notifyDataSetChanged();

                adapter = new SlidesRecyclerViewAdapter(getContext(), this, f);
                gridview.setAdapter(adapter);

            /*imageadapter = new ImageAdapter1(getActivity(), R.layout.galleryitem, galleryItemsList);
            gridview.setAdapter(imageadapter);*/
            } else {
                adapter.notifyDataSetChanged();
            }
        }

    }


    /*class ViewHolder {
        ImageView imageview;
        CheckBox checkbox;

    }*/

    /*public class ImageAdapter1 extends ArrayAdapter<GalleryItem> {
        Context ctx;
        LayoutInflater lInflater;
        ArrayList<GalleryItem> objects;
        ViewHolder viewHolder;
    *//*ImageAdapter1(Context context, ArrayList<GalleryItem> galleryItemList) {
        ctx = context;
        objects = galleryItemList;
        lInflater = (LayoutInflater) ctx
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }*//*

        public ImageAdapter1(Context context, int resourceId, ArrayList<GalleryItem> galleryItemList) {
            super(context, resourceId, galleryItemList);
            // mSelectedItemsIds = new SparseBooleanArray();
            ctx = context;
            objects = galleryItemList;
            lInflater = LayoutInflater.from(context);
        }

        @Override
        public int getCount() {
            return objects.size();
        }

        @Override
        public GalleryItem getItem(int position) {
            return objects.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public void remove(GalleryItem object) {
            System.out.println("removing image pos----------->" + object.position);
            // int pos=object.position;
//            objects.remove(object);
            String imagefilename = object.imgFileName;
            String audioFilename = imagefilename.replace(".png", ".wav");

            File sdCard = Environment.getExternalStorageDirectory();


            File imgDir = new File(sdCard.getAbsolutePath() + "/lokavidya" + "/" + projectName + "/images");
            File audDir = new File(sdCard.getAbsolutePath() + "/lokavidya" + "/" + projectName + "/audio");
            File tmpimgDir = new File(sdCard.getAbsolutePath() + "/lokavidya" + "/" + projectName + "/tmp_images");

            File image_file = new File(imgDir, imagefilename);
            File audio_file = new File(audDir, audioFilename);
            File tmpimage_file = new File(tmpimgDir, imagefilename);


            if (image_file.delete()) {

            }
            if (audio_file.delete()) {

            }
            if (tmpimage_file.delete()) {

            }
            objects.remove(object);
            notifyDataSetChanged();
            count = 0;
            for (int i = 0; i < objects.size(); i++)
                System.out.println("object postion retain------------------>" + objects.get(i).position + "---------" + objects.get(i).imgFileName);
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            View view = convertView;
            if (view == null) {
                view = lInflater.inflate(R.layout.galleryitem, parent, false);
                viewHolder = new ViewHolder();
                viewHolder.imageview = ((ImageView) view.findViewById(R.id.thumbImage));
                viewHolder.checkbox = (CheckBox) view.findViewById(R.id.itemCheckBox);
                view.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) view.getTag();
            }

            GalleryItem p = getProduct(position);

            File sdCard = Environment.getExternalStorageDirectory();


            File imgDir = new File(sdCard.getAbsolutePath() + "/lokavidya" + "/" + projectName + "/images");
            File image_file = new File(imgDir, p.imgFileName);
            Bitmap myBitmap = BitmapFactory.decodeFile(image_file.getAbsolutePath());
            //      ((TextView) view.findViewById(R.id.subgrpname)).setText(p.name);
            viewHolder.imageview.setImageBitmap(myBitmap);
            viewHolder.imageview.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ImageClickCallBack(position);

                }
            });
            //CheckBox cbBuy = (CheckBox) view.findViewById(R.id.itemCheckBox);
            viewHolder.checkbox.setOnCheckedChangeListener(myCheckChangList);
            viewHolder.checkbox.setTag(position);
            viewHolder.checkbox.setChecked(p.box);
            return view;
        }

        GalleryItem getProduct(int position) {
            return ((GalleryItem) getItem(position));
        }

        ArrayList<GalleryItem> getBox() {
            ArrayList<GalleryItem> box = new ArrayList<GalleryItem>();
            for (GalleryItem p : objects) {
                System.out.println("details------>" + p.position + "  " + p.box);

                box.add(p);
            }
            return box;
        }

        CompoundButton.OnCheckedChangeListener myCheckChangList = new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView,
                                         boolean isChecked) {
                getProduct((Integer) buttonView.getTag()).box = isChecked;
                if (isChecked == true) {
                    count++;
                } else {
                    count--;
                }
                getProduct((Integer) buttonView.getTag()).position = (Integer) buttonView.getTag();

            }
        };


    }*/

    /* The following method is used to convert an absolute path to Content Uri.
    The content Uri is needed for the MediaStore.Images.Media.getBitmap() method used above, to pass as the second parameter
     */
    public static Uri getImageContentUri(Context context, String filePath) {
        // String filePath = imageFile.getAbsolutePath();
        Cursor cursor = context.getContentResolver().query(
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                new String[]{MediaStore.Images.Media._ID},
                MediaStore.Images.Media.DATA + "=? ",
                new String[]{filePath}, null);
        if (cursor != null && cursor.moveToFirst()) {
            int id = cursor.getInt(cursor.getColumnIndex(MediaStore.MediaColumns._ID));
            return Uri.withAppendedPath(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, "" + id);
        } else {
            if (new File(filePath).exists()) {
                ContentValues values = new ContentValues();
                values.put(MediaStore.Images.Media.DATA, filePath);
                return context.getContentResolver().insert(
                        MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
            } else {
                return null;
            }
        }
    }

    public static Bitmap addpaddingBitmap(Bitmap unscaledBitmap, int dstWidth, int dstHeight, int maxsize) {
        int width, height;
        int padwidth, padheight;
        int imwidth, imheight;

        width = maxsize;
        height = (int) maxsize * 3 / 4;

        if (dstWidth >= dstHeight) {
            if (height > dstHeight) {
                padheight = height - dstHeight;
                padwidth = 0;
            } else {
                padheight = 0;
                float ratio = (float) dstHeight / (float) height;
                padwidth = (int) (width - width / ratio);
            }
        } else {
            dstHeight = (int) dstHeight * 3 / 4;
            dstWidth = (int) dstWidth * 3 / 4;
            if (width > dstWidth) {
                padheight = 0;
                padwidth = width - dstWidth;
            } else {
                padwidth = 0;
                float ratio = (float) dstWidth / (float) width;
                padheight = (int) (height - height / ratio);
            }
        }

        imwidth = width - padwidth;
        imheight = height - padheight;


        System.out.println("...................................................dstHeight = " + dstHeight);
        System.out.println("...................................................dstWidth = " + dstWidth);
        System.out.println("...................................................padheight = " + padheight);
        System.out.println("...................................................padwidth = " + padwidth);
        System.out.println("...................................................imheight = " + imheight);
        System.out.println("...................................................imwidth = " + imwidth);


        Bitmap scaledBitmap = Bitmap.createScaledBitmap(unscaledBitmap, imwidth, imheight, true);

        Bitmap paddedBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(paddedBitmap);
        canvas.drawARGB(0xFF, 0x00, 0x00, 0x00);
        canvas.drawBitmap(scaledBitmap, padwidth / 2, padheight / 2, null);

        System.out.println("...................................................dstHeight = " + dstHeight);
        System.out.println("...................................................dstWidth = " + dstWidth);
        System.out.println("...................................................padheight = " + padheight);
        System.out.println("...................................................padwidth = " + padwidth);
        System.out.println("...................................................imheight = " + imheight);
        System.out.println("...................................................imwidth = " + imwidth);
        System.out.println("...................................................canwidth = " + canvas.getWidth());
        System.out.println("...................................................canheight = " + canvas.getHeight());

        return paddedBitmap;
    }

}
