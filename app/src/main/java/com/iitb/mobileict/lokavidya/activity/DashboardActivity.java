package com.iitb.mobileict.lokavidya.activity;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.iitb.mobileict.lokavidya.R;
import com.iitb.mobileict.lokavidya.fragment.HomeFragment;
import com.iitb.mobileict.lokavidya.fragment.ProfileFragment;
import com.iitb.mobileict.lokavidya.fragment.CreateProjectFragment;

public class DashboardActivity extends AppCompatActivity implements ProfileFragment.ChangeTab /*extends Activity implements ActionBar.TabListener*/ {

    Fragment projectFramment = null;
    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(false);

        viewPager = findViewById(R.id.viewpager);

        //setupViewPager(viewPager);
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentPagerAdapter pagerAdapter = new ViewPagerAdapter(fragmentManager);
        viewPager.setAdapter(pagerAdapter);

        tabLayout = findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        setupTabIcons();

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                String title;
                switch (position) {
                    case 0:
                        title = "Home";
                        break;
                    case 1:
                        title = "Create Project";
                        break;
                    case 2:
                        title = "Profile";
                        break;
                    default:
                        title = "Dashboard";
                }
                getSupportActionBar().setTitle(title);
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    private void setupTabIcons() {
        tabLayout.getTabAt(0).setIcon(R.drawable.home);
        tabLayout.getTabAt(1).setIcon(R.drawable.create_videos);
        tabLayout.getTabAt(2).setIcon(R.drawable.profile);
    }

    @Override
    public void switchToProject(String projectName) {
        viewPager.setCurrentItem(1);
        PopulateData populateData = (PopulateData) projectFramment;
        populateData.addProject(projectName);
    }

    public interface PopulateData {
        void addProject(String name);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            Fragment fragment = null;
            if (position == 0)
                fragment = new HomeFragment();

            if (position == 1) {
                fragment = new CreateProjectFragment();
                projectFramment = fragment;
            }

            if (position == 2)
                fragment = new ProfileFragment();

            return fragment;
        }

        @Override
        public int getCount() {
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return null;
        }
    }
}
