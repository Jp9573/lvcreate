package com.iitb.mobileict.lokavidya.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.iitb.mobileict.lokavidya.R;
import com.iitb.mobileict.lokavidya.util.Master;

import java.io.File;

import life.knowledge4.videotrimmer.K4LVideoTrimmer;
import life.knowledge4.videotrimmer.interfaces.OnK4LVideoListener;
import life.knowledge4.videotrimmer.interfaces.OnTrimVideoListener;

public class TrimmerActivity extends AppCompatActivity implements OnTrimVideoListener, OnK4LVideoListener {

    private K4LVideoTrimmer mVideoTrimmer;
    private ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trimmer);

        Intent extraIntent = getIntent();
        String path = "";

        if (extraIntent != null) {
            path = extraIntent.getStringExtra(VideoPickerActivity.EXTRA_VIDEO_PATH);
        }

        //setting progressbar
        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setMessage(getString(R.string.trimming_progress));

        mVideoTrimmer = findViewById(R.id.timeLine);
        if (mVideoTrimmer != null) {
            mVideoTrimmer.setMaxDuration(180);
            mVideoTrimmer.setOnTrimVideoListener(this);
            mVideoTrimmer.setOnK4LVideoListener(this);
            String fileName = getVideoName();

            final String videoPath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/lokavidya" + "/" + Master.getCurrentProjectName();
            File file = new File(videoPath, "videos");
            if(!file.exists()) {
                file.mkdir();
            }

            String outputPath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + "lokavidya" + "/" + Master.getCurrentProjectName() + "/images/" + fileName;
            mVideoTrimmer.setDestinationPath(outputPath);
            mVideoTrimmer.setVideoURI(Uri.parse(path));
            mVideoTrimmer.setVideoInformationVisibility(true);
        }
    }

    public String getVideoName() {
        String newImg;
        File sdCard = Environment.getExternalStorageDirectory();
        File imgDir = new File(sdCard.getAbsolutePath() + "/" + "lokavidya" + "/" + Master.getCurrentProjectName() + "/images");

        if (!imgDir.exists()) {
            imgDir.mkdirs();
        }

        File file[] = imgDir.listFiles();
        newImg = Master.getCurrentProjectName() + "." + String.valueOf(file.length + 1) + ".mp4";

        if (file.length != 0) {
            boolean fileExists = false;

            int countImg[] = new int[file.length];

            for (int i = 0; i < file.length; i++) {
                String imgName = file[i].getName();
                imgName = imgName.replace(".mp4", "");
                String splitImgName[] = imgName.split("\\.");
                try {
                    countImg[i] = Integer.parseInt(splitImgName[1]);
                }catch (Exception e) {
                    countImg[i] = Integer.parseInt(splitImgName[1].substring(0,splitImgName[1].indexOf("M")));
                }
                if (newImg.equals(file[i].getName())) {
                    fileExists = true;
                }
            }


            if (fileExists == true) {
                int large = largestAndSmallest(countImg);

                newImg = Master.getCurrentProjectName() + "." + String.valueOf(large + 1) + ".mp4";
            }
        }
        return newImg;
    }

    public int largestAndSmallest(int[] numbers) {
        int largest = Integer.MIN_VALUE;
        int smallest = Integer.MAX_VALUE;
        for (int number : numbers) {
            if (number > largest) {
                largest = number;
            } else if (number < smallest) {
                smallest = number;
            }
        }
        return largest;
    }

    @Override
    public void onTrimStarted() {
        mProgressDialog.show();
    }

    @Override
    public void getResult(final Uri uri) {
        mProgressDialog.cancel();

        /*runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(TrimmerActivity.this, getString(R.string.video_saved_at, uri.getPath()), Toast.LENGTH_SHORT).show();
            }
        });
        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
        intent.setDataAndType(uri, "video/mp4");
        startActivity(intent);*/
        finish();
    }

    @Override
    public void cancelAction() {
        mProgressDialog.cancel();
        mVideoTrimmer.destroy();
        finish();
    }

    @Override
    public void onError(final String message) {
        mProgressDialog.cancel();

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(TrimmerActivity.this, message, Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onVideoPrepared() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(TrimmerActivity.this, "onVideoPrepared", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
