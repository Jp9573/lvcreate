package com.iitb.mobileict.lokavidya.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.iitb.mobileict.lokavidya.R;
import com.iitb.mobileict.lokavidya.network.NetworkCommunicator;
import com.iitb.mobileict.lokavidya.network.NetworkException;
import com.iitb.mobileict.lokavidya.network.NetworkResponse;
import com.iitb.mobileict.lokavidya.util.Master;

import org.json.JSONException;
import org.json.JSONObject;

public class ResetPasswordActivity extends AppCompatActivity {

    EditText passwordEdt;
    EditText confirmPasswordEdt;
    Button mConfirmButton;
    String uuid;
    private NetworkCommunicator networkCommunicator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password);

        Bundle args = getIntent().getExtras();
        uuid = args.getString("uuid");

        passwordEdt = (EditText) findViewById(R.id.et_enter_new_password);
        confirmPasswordEdt = (EditText) findViewById(R.id.et_re_enter_to_confirm);
        mConfirmButton = (Button) findViewById(R.id.btn_confirm);

        networkCommunicator = NetworkCommunicator.getInstance();

        mConfirmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!uuid.equals("")) {
                    if (passwordEdt.getText().toString().length() > 0) {
                        String pass = passwordEdt.getText().toString();
                        String cnfPass = confirmPasswordEdt.getText().toString();

                        if (pass.equals(cnfPass)) {
                            setNewPassword(pass);
                        } else {
                            confirmPasswordEdt.setError("Password must be same!");
                        }
                    } else {
                        passwordEdt.setError("Please enter valid password!");
                    }
                }else {
                    Toast.makeText(ResetPasswordActivity.this, getString(R.string.toast_technical_issue), Toast.LENGTH_LONG).show();
                    mConfirmButton.setEnabled(false);
                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (networkCommunicator == null) {
            networkCommunicator = NetworkCommunicator.getInstance();
        }
    }

    void setNewPassword(String password) {
        Master.showProgressDialog(ResetPasswordActivity.this, "Setting up new password for you");
        JSONObject userJsonObject = new JSONObject();
        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("uuid", uuid);
            jsonObject.put("password", password);
            userJsonObject.put("user", jsonObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        networkCommunicator.data(Master.getResetPasswordAPI(),
                Request.Method.POST,
                userJsonObject,
                false, new NetworkResponse.Listener() {

                    @Override
                    public void onResponse(Object result) {
                        Master.dismissProgressDialog();
                        String response = (String) result;
                        JSONObject obj = null;
                        try {
                            obj = new JSONObject(response);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        if (obj != null) {
                            try {
                                String res = obj.getString("status");
                                String message = obj.getString("message");

                                if (res.equals("200") && message.equals("Password Updated")) {
                                    Intent intent = new Intent(ResetPasswordActivity.this, WelcomeActivity.class);
                                    finishAffinity();
                                    startActivity(intent);
                                }else {
                                    Toast.makeText(ResetPasswordActivity.this, getString(R.string.toast_technical_issue), Toast.LENGTH_LONG).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }, new NetworkResponse.ErrorListener() {
                    @Override
                    public void onError(NetworkException error) {
                        Master.dismissProgressDialog();
                        error.printStackTrace();
                    }


                }, "ResetPassword", ResetPasswordActivity.this);
    }
}
